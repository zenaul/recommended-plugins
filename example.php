<?php
/**
 * Constructor args
 *
 * @param string    $text_domain your plugin text domain.
 * @param string    $parent_menu_slug the menu slug name where the "Recommendations" submenu will appear.
 * @param string    $menu_page_slug an unique page name for the submenu.
 * @param string    $menu_capability this is takes a capability which will be used to determine whether or not a page is included in the menu.
 * @param int       $priority Submenu priority adjust.
 * @param string    $assets_url your plugin assets url.
 * @param string    $hook_suffix use it to load this library assets only to the recommedded plugins page. Not into the whol admin area.
 *
 */

$get_instance = HTRP_Recommended_Plugins::instance( 
    array( 
        'text_domain'       => '', 
        'parent_menu_slug'  => '', 
        'menu_capability'   => 'manage_options', 
        'menu_page_slug'    => 'recommendations',
        'priority'          => '',
        'assets_url'        => '',
        'hook_suffix'       => ''
    )
);


// Free Plugins
$get_instance->add_new_tab( array(

    'title' => esc_html__( 'Recommended', 'text-domain' ),
    'active' => true,
    'plugins' => array(
        array(
            'slug'      => 'woolentor-addons',
            'location'  => 'woolentor_addons_elementor.php',
            'name'      => esc_html__( 'WooLentor', 'text-domain' )
        ),
        array(
            'slug'      => 'wc-builder',
            'location'  => 'wc-builder.php',
            'name'      => esc_html__( 'WC Builder', 'text-domain' )
        ),
        array(
            'slug'      => 'ever-compare',
            'location'  => 'ever-compare.php',
            'name'      => esc_html__( 'EverCompare', 'text-domain' )
        ),
        array(
            'slug'      => 'quickswish',
            'location'  => 'quickswish.php',
            'name'      => esc_html__( 'QuickSwish', 'text-domain' )
        ),
        array(
            'slug'      => 'whols',
            'location'  => 'whols.php',
            'name'      => esc_html__( 'Whols', 'text-domain' )
        ),
        array(
            'slug'      => 'just-tables',
            'location'  => 'just-tables.php',
            'name'      => esc_html__( 'JustTables', 'text-domain' )
        ),
        array(
            'slug'      => 'wc-multi-currency',
            'location'  => 'wcmilticurrency.php',
            'name'      => esc_html__( 'Multi Currency', 'text-domain' )
        )
    )

) );

// Pro plugins
$get_instance->add_new_tab(array(
    'title' => esc_html__( 'You May Also Like', 'text-domain' ),
    'plugins' => array(

        array(
            'slug'      => 'woolentor-addons-pro',
            'location'  => 'woolentor_addons_pro.php',
            'name'      => esc_html__( 'WooLentor Pro', 'text-domain' ),
            'link'      => 'https://hasthemes.com/plugins/woolentor-pro-woocommerce-page-builder/',
            'author_link'=> 'https://hasthemes.com/',
            'description'=> esc_html__( 'WooLentor is one of the most popular WooCommerce Elementor Addons on WordPress.org. It has been downloaded more than 672,148 times and 60,000 stores are using WooLentor plugin. Why not you?', 'text-domain' ),
        ),

        array(
            'slug'      => 'just-tables-pro',
            'location'  => 'just-tables-pro.php',
            'name'      => esc_html__( 'JustTables Pro', 'text-domain' ),
            'link'      => 'https://hasthemes.com/wp/justtables/',
            'author_link'=> 'https://hasthemes.com/',
            'description'=> esc_html__( 'JustTables is an incredible WordPress plugin that lets you showcase all your WooCommerce products in a sortable and filterable table view. It allows your customers to easily navigate through different attributes of the products and compare them on a single page. This plugin will be of great help if you are looking for an easy solution that increases the chances of landing a sale on your online store.', 'text-domain' ),
        ),

        array(
            'slug'      => 'whols-pro',
            'location'  => 'whols-pro.php',
            'name'      => esc_html__( 'Whols Pro', 'text-domain' ),
            'link'      => 'https://hasthemes.com/plugins/whols-woocommerce-wholesale-prices/',
            'author_link'=> 'https://hasthemes.com/',
            'description'=> esc_html__( 'Whols is an outstanding WordPress plugin for WooCommerce that allows store owners to set wholesale prices for the products of their online stores. This plugin enables you to show special wholesale prices to the wholesaler. Users can easily request to become a wholesale customer by filling out a simple online registration form. Once the registration is complete, the owner of the store will be able to review the request and approve the request either manually or automatically.', 'text-domain' ),
        ),

        array(
            'slug'      => 'multicurrencypro',
            'location'  => 'multicurrencypro.php',
            'name'      => esc_html__( 'Multi Currency Pro for WooCommerce', 'text-domain' ),
            'link'      => 'https://hasthemes.com/plugins/multi-currency-pro-for-woocommerce/',
            'author_link'=> 'https://hasthemes.com/',
            'description'=> esc_html__( 'Multi-Currency Pro for WooCommerce is a prominent currency switcher plugin for WooCommerce. This plugin allows your website or online store visitors to switch to their preferred currency or their country’s currency.', 'text-domain' ),
        ),

        array(
            'slug'      => 'email-candy-pro',
            'location'  => 'email-candy-pro.php',
            'name'      => esc_html__( 'Email Candy Pro', 'text-domain' ),
            'link'      => 'https://hasthemes.com/plugins/email-candy-pro/',
            'author_link'=> 'https://hasthemes.com/',
            'description'=> esc_html__( 'Email Candy is an outstanding WordPress plugin that allows you to customize the default WooCommerce email templates and give a professional look to your WooCommerce emails. If you are tired of using the boring design of WooCommerce emails and want to create customized emails, then this plugin will come in handy.', 'text-domain' ),
        ),
    )
));

// Free and pro plugins combile
$get_instance->add_new_tab(array(
    'title' => esc_html__( 'Others', 'text-domain' ),
    'plugins' => array(

        array(
            'slug'      => 'ht-mega-for-elementor',
            'location'  => 'htmega_addons_elementor.php',
            'name'      => esc_html__( 'HT Mega', 'text-domain' )
        ),

        array(
            'slug'      => 'ht-slider-for-elementor',
            'location'  => 'ht-slider-for-elementor.php',
            'name'      => esc_html__( 'HT Slider For Elementor', 'text-domain' )
        ),

        array(
            'slug'      => 'wp-plugin-manager',
            'location'  => 'plugin-main.php',
            'name'      => esc_html__( 'WP Plugin Manager', 'text-domain' )
        ),

        array(
            'slug'      => 'ht-contactform',
            'location'  => 'contact-form-widget-elementor.php',
            'name'      => esc_html__( 'HT Contact Form 7', 'text-domain' )
        ),

        array(
            'slug'      => 'ht-wpform',
            'location'  => 'wpform-widget-elementor.php',
            'name'      => esc_html__( 'HT WPForms', 'text-domain' )
        ),

        array(
            'slug'      => 'hashbar-wp-notification-bar',
            'location'  => 'init.php',
            'name'      => esc_html__( 'HashBar', 'text-domain' )
        ),

        array(
            'slug'      => 'ht-menu-lite',
            'location'  => 'ht-mega-menu.php',
            'name'      => esc_html__( 'HT Menu', 'text-domain' )
        ),

        array(
            'slug'      => 'htmega-pro',
            'location'  => 'htmega_pro.php',
            'name'      => esc_html__( 'HT Mega Pro', 'text-domain' ),
            'link'      => 'https://hasthemes.com/plugins/ht-mega-pro/',
            'author_link'=> 'https://hasthemes.com/',
            'description'=> esc_html__( 'HTMega is an absolute addon for elementor that includes 80+ elements & 360 Blocks with unlimited variations. HT Mega brings limitless possibilities. Embellish your site with the elements of HT Mega.', 'text-domain' ),
        ),

        array(
            'slug'      => 'hashbar-pro',
            'location'  => 'init.php',
            'name'      => esc_html__( 'HashBar Pro', 'text-domain' ),
            'link'      => 'https://hasthemes.com/plugins/wordpress-notification-bar-plugin/',
            'author_link'=> 'https://hasthemes.com/',
            'description'=> esc_html__( 'HashBar is a WordPress Notification / Alert / Offer Bar plugin which allows you to create unlimited notification bars to notify your customers. This plugin has option to show email subscription form (sometimes it increases up to 500% email subscriber), Offer text and buttons about your promotions. This plugin has the options to add unlimited background colors and images to make your notification bar more professional.', 'text-domain' ),
        ),

    )
));